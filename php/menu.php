<?php

//step2. read from mysql
$connection = mysqli_connect("localhost","root","root","phpcart");
mysqli_query($connection,"set names 'utf8'");
//mysqli_set_charset($connection,"utf8");
if ( mysqli_connect_errno()){
    die("Can't establish connection from database, ".mysqli_connect_error());
}

$query = "select * from product ";
$result = mysqli_query($connection,$query);
if ( !$result){
    die("can't execute query ");
}
?>

<?php
    $data =array();
    //2018.06.12
    //把row轉成json格式,產生array形式,每一個element是Dictionary
    while( $row = mysqli_fetch_assoc($result)){
        $product = array(); //Dictionary
        $product["productid"] = $row["productid"];
        $product["categoryid"] = $row["categoryid"];
        $product["productname"] = $row["productname"];
        $product["productprice"] = $row["productprice"];
        $product["productimages"] = $row["productimages"];
        $product["description"] = $row["description"];
        //加到data array中
        $data[] = $product; //addObject

    }

    //轉json.
    echo json_encode($data, JSON_UNESCAPED_SLASHES);
    mysqli_free_result($result);
?>

<?php
mysqli_close($connection);
?>
