//
//  Note.swift
//  MyShopCart
//
//  Created by 劉家維 on 2018/8/7.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import Foundation
import UIKit

class product: Codable {
    
    var productid:String?
    var categoryid:String?
    var productname:String?
    var productprice:String?
    var productimages:String?
    var description:String?
}

class orderdetail: Codable {
    
    var productid:String?
    var productname:String?
    var productprice:String?
    var quantity:String?
    
}

class orders : Codable {
    var orderid:String?
    var total:String?
    var customername:String?
    var customeremail:String?
    var customeraddress:String?
    var customerphone:String?
    
    var results:[orderdetail]!
    
    
    func fetchDetail( byId productid: String )->orderdetail!
    {
        var answer: orderdetail! = nil
        for eachDetail:orderdetail in self.results
        {
            if eachDetail.productid == productid {
                answer =  eachDetail
                break
            }
        }
        return answer
    }
    
}


/////////////////////////////////////////////////////


class TheModel {
    class func TheSendOrder() {
        
        let url = URL(string: "http://localhost:8888/test.php")!
        var request = URLRequest(url: url)
        request.httpMethod = "post"
        
        var str:String =  ""
        if let appDelegate:AppDelegate = UIApplication.shared.delegate as? AppDelegate
        {
            if let orderdetail:[orderdetail] = appDelegate.cart.results
            {
                str = "customername=\(appDelegate.cart.customername!)"
                str = str + "&"
                str = str + "customeremail=\(appDelegate.cart.customeremail!)"
                str = str + "&"
                str = str + "customeraddress=\(appDelegate.cart.customeraddress!)"
                str = str + "&"
                str = str + "customerphone=\(appDelegate.cart.customerphone!)"
                str = str + "&"
                str = str + "total=\(appDelegate.cart.total!)"
                
                for index in 0..<orderdetail.count {
                    
                    let each_detail:orderdetail = orderdetail[ index ]
                    
                    str = str + "&"
                    str = str + "productid[]=\(each_detail.productid!)"
                    str = str + "&"
                    str = str + "productname[]=\(each_detail.productname!)"
                    str = str + "&"
                    str = str + "productprice[]=\(each_detail.productprice!)"
                    str = str + "&"
                    str = str + "quantity[]=\(each_detail.quantity!)"
                    
                }
            } else {
                return
            }
        }
        
        
        request.httpBody = str.data(using: .utf8)
        
        
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
        let dataTask = session.dataTask(with: request) { (dataUw, responseUw, errUw) in
            if let err = errUw {
                print("err: \(err)")
            }
            else if let data = dataUw {
                do{
                    if let dict:[String:Any] = try JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
                    {
                        if let orderdetail = dict["orderdetail"] as? [ [String:Any] ]
                        {
                            for index in 0..<orderdetail.count {
                                let each_detail:[String:Any] = orderdetail[ index ]
                                print("\(each_detail["productname"]!)" , "\(each_detail["quantity"]!)"  , separator:" "  )
                            }
                        }
                    }
                } catch {
                    print("JSONSerialization.jsonObject error: \(error)")
                }
            }
        }
        dataTask.resume()
    }
}
