//
//  ProductViewController.swift
//  MyShopCart
//
//  Created by 劉家維 on 2018/8/7.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import UIKit



class ProductViewController: UIViewController {
    var number :Int = 0
    var dataProduct: product!
    var cart:orders!
    
    @IBOutlet weak var productnameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var productpriceLabel: UILabel!
    
    @IBOutlet weak var productImageView: UIImageView!
    
    @IBOutlet weak var quantityLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.cart = AppDelegate.theOrders()
        
        if let theDetail:orderdetail = self.cart.fetchDetail(byId: dataProduct.productid!)
        {
            self.quantityLabel.text = "\(theDetail.quantity!)"
            
        }
        
        self.productnameLabel.text = self.dataProduct.productname
        self.productpriceLabel.text = self.dataProduct.productprice
        self.descriptionLabel.text = self.dataProduct.description
    
        queryImage()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBAction func quantityStepper(_ sender: UIStepper) {
        number = Int(sender.value)
        quantityLabel.text = String(number)
        
    }
    
    
    @IBAction func inCartBtnPressed(_ sender: UIBarButtonItem) {
        
        guard let newQuantity = Int(self.quantityLabel.text!) else {
            return
        }
        
        if let theDetail:orderdetail = self.cart.fetchDetail(byId: dataProduct.productid!){
            
            if newQuantity > 0 {
                theDetail.quantity = String(newQuantity)
            } else {
                var indexFound:Int! = nil
                for index in 0..<(self.cart.results.count) {
                    if self.cart.results[ index ].productid == dataProduct.productid {
                        indexFound = index
                        break
                    }
                }
                if indexFound != nil {
                    self.cart.results.remove(at: indexFound)
                }
            }
            
        } else {
            if newQuantity > 0 {
                let pResult = orderdetail()
                pResult.productid = self.dataProduct.productid
                pResult.productname = self.dataProduct.productname
                pResult.productprice = self.dataProduct.productprice
                pResult.quantity = String(number)
                self.cart.results.append(pResult)
                
            }
        }
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func queryImage() {
        
        let productimage : String = dataProduct.productimages!
            let url = "http://localhost:8888/proimg/\(productimage)"
                let imageUrl = URL(string: url)
                let request = URLRequest(url: imageUrl!)
                let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
                let task = session.dataTask(with: request) { (data, response, error) in
                    if let error = error {
                        print("error \(error)")
                    }
                    if let imageData = data {
                        DispatchQueue.main.async {
                            let image = UIImage(data: imageData)
                            self.productImageView.image = image
                        }
                    }
                }
                task.resume()
        
    }
    
    /////////////
}
