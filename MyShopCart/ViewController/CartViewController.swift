//
//  CartViewController.swift
//  MyShopCart
//
//  Created by 劉家維 on 2018/8/7.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {
    
    var total : Int?
    var cart:orders!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.total = 0
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.rowHeight = 180
        
        
        self.tableView.reloadData()
     
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if AppDelegate.theOrders().total == nil {
            totalLabel.text = ""
        }
        
        self.tableView.reloadData()
    }

   
    
//////////////////////
}

extension CartViewController: UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppDelegate.theOrders().results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell", for: indexPath) as! CartTableViewCell
        let indexPaths = AppDelegate.theOrders().results[indexPath.row]
        cell.cartnameLabel.text = indexPaths.productname
        cell.cartpriceLabel.text = indexPaths.productprice
        cell.cartquantityLabel.text = indexPaths.quantity
        
        //Prepare total.
        
        if cell.cartpriceLabel.text != nil ,cell.cartquantityLabel.text != nil {
            let price = Int(cell.cartpriceLabel.text!)
            let quantity = Int(cell.cartquantityLabel.text!)
            let total = price! * quantity!
            self.total =  self.total! + total
        }
        totalLabel.text = String(self.total!) + "元"
        
        return cell
    }
    
}

