//
//  ViewController.swift
//  MyShopCart
//
//  Created by 劉家維 on 2018/8/7.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    var dataMenu = [product]()
    var cart:orders!
    @IBOutlet weak var tableView: UITableView!
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        queryFromServe()
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.tableView.reloadData()
    }
    
    
    //MARK: - Prepare link MySQL.
    func queryFromServe() {
        let url = URL(string: "http://localhost:8888/menu.php")!
        let request = URLRequest(url: url)
        let session = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print("error \(error)")
            }
//            if let d = data {
//                let jsonText = String(data: d, encoding: .utf8)
//                print(jsonText!)
//            }
            
            let decoder = JSONDecoder()
            if let jsonData = data {
                do{
                    let result = try decoder.decode([product].self, from: jsonData)
                    self.dataMenu = result
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                } catch {
                    print("error json paring\(error)")
                }
            }
        }
        task.resume()
    }
    
    //MARK: - Prepare Segue.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "menuToPruct" {
            let productVC = segue.destination as! ProductViewController
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let dataMenu = self.dataMenu[indexPath.row]
                productVC.dataProduct = dataMenu
            }
            
        }
    }
    

    @IBAction func toCartBtnPressed(_ sender: UIBarButtonItem) {
        
    }
    
    
    
    /////////
}

extension MenuViewController: UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - UITableViewDataSource Methods.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as? MenuTableViewCell
        let indexPaths = dataMenu[indexPath.row]
        cell?.menuNameLabel?.text = indexPaths.productname
        cell?.menuPriceLabel?.text = indexPaths.productprice
        cell?.quantityImage.image = UIImage(named: "16-cube-orange")
        
        for _ in 0..<(AppDelegate.theOrders().results.count) {
            AppDelegate.theOrders().results.forEach { (orderdetail) in
                let productid = orderdetail.productid
                let quantity = Int(orderdetail.quantity!)
                
                if  indexPaths.productid == productid && quantity! > 0 {
                    cell?.quantityImage.image = UIImage(named: "16-cube-green")
                }
            }
        }
        
        
        return cell!
    }
    
    //MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("\(indexPath.row)")
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
   

    
    /////
}


