//
//  PostViewController.swift
//  MyShopCart
//
//  Created by 劉家維 on 2018/8/7.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import UIKit

class PostViewController: UIViewController {
    var total:Int = 0
    var cartPost:orders!
    @IBOutlet weak var customerNameField: UITextField!
    @IBOutlet weak var customerEMailField: UITextField!
    @IBOutlet weak var customerAddressField: UITextField!
    @IBOutlet weak var customerPhoneField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customerNameField.becomeFirstResponder()
        
        //取array 內的值.
        self.total = 0
        AppDelegate.theOrders().results.forEach { (orderdetail) in
            let price = Int(orderdetail.productprice!)
            let quantity = Int(orderdetail.quantity!)
            let total = price! * quantity!
            self.total =  self.total + total
            let allTotal = self.total
            AppDelegate.theOrders().total = String(allTotal)
            
            print("total= \(allTotal)")
            
        }
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - Prepare Post.
    @IBAction func PostBtnPressed(_ sender: UIBarButtonItem) {
        
        let data = orders()
        data.customername = AppDelegate.theOrders().customername
        data.customeremail = AppDelegate.theOrders().customeremail
        data.customeraddress = AppDelegate.theOrders().customeraddress
        data.customerphone = AppDelegate.theOrders().customerphone
        data.total = AppDelegate.theOrders().total
        data.results = AppDelegate.theOrders().results
        
        self.cartPost = data
        
        guard data.customername != nil && data.customeremail != nil && data.customeraddress != nil && data.customerphone != nil && data.total != nil && data.results != nil   else {
            //            assertionFailure(" isEmpty")
            let alert = UIAlertController(title: "警告", message: "不可空白", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
                //..
            }
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
            
            return
        }
        
        let alert = UIAlertController(title: "", message: "感謝\(String(describing: data.customername!))的購買", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { (UIAlertAction) in
            AppDelegate.theOrders().total = nil
            AppDelegate.theOrders().customername = nil
            AppDelegate.theOrders().customeremail = nil
            AppDelegate.theOrders().customeraddress = nil
            AppDelegate.theOrders().customerphone = nil
            AppDelegate.theOrders().results = []
            if AppDelegate.theOrders().customername == nil {
                self.customerNameField.text = ""
            }
            if AppDelegate.theOrders().customeremail == nil {
                self.customerEMailField.text = ""
            }
            if AppDelegate.theOrders().customeraddress == nil {
                self.customerAddressField.text = ""
            }
            if AppDelegate.theOrders().customerphone == nil {
                self.customerPhoneField.text = ""
            }
            
        self.navigationController?.popToRootViewController(animated: true)
            
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
        TheModel.TheSendOrder()
        
    }
    
    
    //MARK: - UITextField Methods.
    
    @IBAction func customerNameBtnPressed(_ sender: UITextField) {
        
        AppDelegate.theOrders().customername = customerNameField.text
    }
    
    @IBAction func customerEmailBtnPressed(_ sender: UITextField) {
        
        AppDelegate.theOrders().customeremail = customerEMailField.text
        
    }
    @IBAction func customerAddressBtnPressed(_ sender: UITextField) {
        
        AppDelegate.theOrders().customeraddress = customerAddressField.text
        
        
    }
    
    @IBAction func customerPhoneBtnPressed(_ sender: UITextField) {
        
        AppDelegate.theOrders().customerphone = customerPhoneField.text
        
        
    }
    
    ////////
}

